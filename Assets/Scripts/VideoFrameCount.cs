﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class VideoFrameCount : MonoBehaviour
{

    [SerializeField] GameObject skipButton;
    [SerializeField] GameObject playGameButton;
    [SerializeField] GameObject switchVideoOff;


    public VideoPlayer VideoPlayer; // Drag & Drop the GameObject holding the VideoPlayer component

    void Start()
    {
        VideoPlayer.loopPointReached += LoadScene;
    }

    void LoadScene(VideoPlayer vp)
    {
        skipButton.SetActive(false);
        playGameButton.SetActive(true);
        switchVideoOff.SetActive(false);
    }
}