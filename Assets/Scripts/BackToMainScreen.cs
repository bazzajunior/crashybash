﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BackToMainScreen : MonoBehaviour
{
    public void ReturnToMainScreen()
    {
        SceneManager.LoadScene(0);
    }
}
