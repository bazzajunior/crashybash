﻿using UnityEngine;
using EasyMobile;

public class EasyMobileLaunch : MonoBehaviour
{
    private void Start()
    {
        RequestRating();
    }

    public void RequestRating()
    {
        if (StoreReview.CanRequestRating())
            StoreReview.RequestRating();
    }
}