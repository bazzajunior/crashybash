﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadGameWithPlayerPrefs : MonoBehaviour
{
    public void CheckPlayerPrefs()
    {
        if  (PlayerPrefs.GetInt("SeenTutorial") == 1)
        {
            SceneManager.LoadScene(1);
            SceneManager.LoadScene(2, LoadSceneMode.Additive);
        }
        else {
            SceneManager.LoadScene(3);
        }
    }
}