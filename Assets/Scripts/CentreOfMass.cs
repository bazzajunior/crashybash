﻿using UnityEngine;
using System.Collections;

public class CentreOfMass : MonoBehaviour {
	public Vector3 com;
	public Rigidbody rb;
	void Start() {
		rb = GetComponent<Rigidbody>();
		rb.centerOfMass = com;
	}
}