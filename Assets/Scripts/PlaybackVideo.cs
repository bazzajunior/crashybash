﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;


public class PlaybackVideo : MonoBehaviour
{

    [SerializeField] GameObject playButton;
    [SerializeField] GameObject skipButton;
    [SerializeField] GameObject videoCover;

    VideoPlayer videoPlayer;

    public void VideoPlayback()
    {
        videoPlayer = GetComponent<VideoPlayer>();
        videoPlayer.Play();
        skipButton.SetActive(true);
        playButton.SetActive(false);
        videoCover.SetActive(false);
    }
}