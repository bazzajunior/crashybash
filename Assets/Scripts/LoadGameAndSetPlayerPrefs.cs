﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadGameAndSetPlayerPrefs : MonoBehaviour
{
    // Let's confirm that we have seen the tutorial (this scene)
    void Start()
    {
        PlayerPrefs.SetInt("SeenTutorial", 1); 
    }

    public void LoadStartScene()
    {
        SceneManager.LoadScene(1);
        SceneManager.LoadScene(2, LoadSceneMode.Additive);
    }
}
