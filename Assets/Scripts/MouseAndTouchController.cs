﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Touch;

public class MouseAndTouchController : MonoBehaviour
{
    [SerializeField] WheelCollider rightFrontWheel;
    [SerializeField] WheelCollider leftFrontWheel;
    [SerializeField] WheelCollider rightRearWheel;
    [SerializeField] WheelCollider leftRearWheel;
    [SerializeField] float maxTorque = 0.0f;
    [SerializeField] float maximum = 15f;
    [SerializeField] float raycastDistance = 100f;
    [SerializeField] Vector3 clickPosition;
    [SerializeField] GameObject tankChassis;

    Animation anim;
    AudioSource audioData;
    public float smooth = 9f;
    [SerializeField] LayerMask layerMask;

    public void Start()
    {
        anim = tankChassis.GetComponent<Animation>();
        audioData = GetComponent<AudioSource>();
    }

    public void CheckForRaycast(LeanFinger finger)
    {
        RaycastHit hitInfo;
        Ray ray = Camera.main.ScreenPointToRay(finger.ScreenPosition);

        if (Physics.Raycast(ray, out hitInfo, raycastDistance, layerMask))
        {
                clickPosition = hitInfo.point - transform.position;

                Quaternion rotation = Quaternion.LookRotation(clickPosition, Vector3.up);
                rotation.x = 0;
                rotation.z = 0;
                transform.rotation = Quaternion.Lerp(transform.rotation, rotation, smooth * Time.deltaTime);
        }
    }

    public void OnFingerDown()
    {
        //set the forward momentum
        maxTorque = maximum;
        rightFrontWheel.motorTorque = maxTorque;
        leftFrontWheel.motorTorque = maxTorque;
        rightRearWheel.motorTorque = maxTorque;
        leftRearWheel.motorTorque = maxTorque;
        
        //set the tank track animation
        anim["Default Take"].speed = 1.5f;
        anim.Play("Default Take");
        anim["Default Take"].wrapMode = WrapMode.Loop;
        
        //playback the audio
        audioData.Play(0);
        audioData.volume = 1;

    }
    public void OnFingerUp()
    {
        maxTorque = 0f;
        rightFrontWheel.motorTorque = maxTorque;
        leftFrontWheel.motorTorque = maxTorque;
        rightRearWheel.motorTorque = maxTorque;
        leftRearWheel.motorTorque = maxTorque;
        anim.Stop("Default Take");
        audioData.Stop();
    }

    void OnEnable()
    {
        LeanTouch.OnFingerTap += HandleFingerTap;
    }

    void OnDisable()
    {
        LeanTouch.OnFingerTap -= HandleFingerTap;
    }

    void HandleFingerTap(LeanFinger finger)
    {
        if (finger.IsOverGui)
        {
            Debug.Log("You just tapped the screen on top of the GUI!");
        }
    }
}
